require('./errorHelper')

const amqp = require('amqplib');
const uuid = require('uuid/v4');

const callFunc = require('./serverless').callFunc

const REQUEST_TIMEOUT = 30000;
const DEAD_LETTERS_QUEUE_NAME = 'dead_letters'
let connection = null
let channel = null
let replyToQueue = null

let corid_promise_map = {}
let corid_rejectTimeout_map = {}

const connect = async (url,startRPC = false) =>{
    if(!connection){
       connection = await amqp.connect(url);
       channel = await connection.createChannel();
       await _makeDeadLetterQueue();
       if(startRPC) await _startRPCConsumer();
    }
    return connection
}

const _makeDeadLetterQueue = async () => {
    await channel.assertQueue(DEAD_LETTERS_QUEUE_NAME, {
        durable: false,
        arguments: {
            "x-dead-letter-exchange":"",
            "x-dead-letter-routing-key": DEAD_LETTERS_QUEUE_NAME
        }
    });
}

const consumeQueue = async (queueName,responseFn) =>{
    if(!channel) throw new Error('Not Connected');
    
    await channel.assertQueue(queueName, {
        durable: false,
        arguments: {
            "x-dead-letter-exchange":"",
            "x-dead-letter-routing-key": DEAD_LETTERS_QUEUE_NAME
        }
    });
    await channel.prefetch(10);
    await channel.consume(queueName, async (msg)=>{

        try{
            const payload = JSON.parse(msg.content.toString());
            console.log('\n========= RECEIVED =================');
            console.log(msg.properties.correlationId,payload);
            console.log('====================================');
            const isRPC = !!msg.properties.replyTo;
            const response = await responseFn(payload);
            
            // 1. Respond for RPC
            if(isRPC){
                await channel.sendToQueue(
                    msg.properties.replyTo,
                    Buffer.from(JSON.stringify(response)),
                    {correlationId: msg.properties.correlationId}
                );
                console.log('\n========= RESPONDED ================');
                console.log(msg.properties.correlationId,response);
                console.log('====================================');
            }

            // 2. Message acking and dead letter queuing
            if(response.error && !isRPC){
                 // Send to dead letter queue
                await channel.nack(msg,false,false);
            }else{
                await channel.ack(msg);
            }

        }catch(e){ // Unexpected Error
            console.error(e);
            await channel.nack(msg);
        }
       
    });
    
}


const _startRPCConsumer = async () => {
    const replyToQueueObj = await channel.assertQueue('', {
        exclusive: true,
        expires: 5000,
        autodelete: true,
   });
   replyToQueue = replyToQueueObj.queue;
   channel.consume(replyToQueue, _RPCConsumeCallback);
}

const _RPCConsumeCallback = async (msg) => {
        const corr = msg.properties.correlationId;
        const corrPromise = corid_promise_map[corr];
        const payload = JSON.parse(msg.content.toString());
        console.log(corr,`\nReceived`,payload);
        await channel.ack(msg);
        if(corrPromise) {
            if(!payload.error){
                corrPromise.resolve(payload.data);
            }else{
                corrPromise.reject(payload.error);
            }
            delete corid_promise_map[corr];
            delete corid_rejectTimeout_map[corr];
        }else{
            console.error(corr,'Could not find the resolve function');
        }
}

const makeRPC = (queue,payload) => {
    //IN  {queue:String!,payload:{data,operation}}
    //OUT (resolve with response from rpc) OR (reject with error from rpc)
    return new Promise(async (resolve,reject)=>{
        if(!channel || !replyToQueue) reject('Not Connected')
        else{
            
            await channel.assertQueue(queue, {
                durable: false,
                arguments: {
                    "x-dead-letter-exchange":"",
                    "x-dead-letter-routing-key": DEAD_LETTERS_QUEUE_NAME
                }
            });
            // OPTIONAL :: Will also be done by the worker
            
            const corr = uuid();
            
            corid_promise_map[corr] = {resolve,reject};
            
            channel.sendToQueue(queue, new Buffer(JSON.stringify(payload)), {
                correlationId: corr,
                replyTo: replyToQueue,
            });
            console.log(corr,'\nSent MSG on Queue:',queue,' Payload:',payload);
            
            // console.log(corid_rejectTimeout_map);
            // Reject the promise after REQUEST_TIMEOUT ms
            corid_rejectTimeout_map[corr] = setTimeout(()=>{
                reject('Request Timed Out');
                delete corid_promise_map[corr];
                delete corid_rejectTimeout_map[corr];
            },REQUEST_TIMEOUT);
        }
    })
}

const fireTask = (queue,payload) => {
    //IN  {queue:String!,payload:{data,operation}}
    //OUT resolves to true/false immediately
    return new Promise(async (resolve,reject)=>{
        if(!channel) reject('Not Connected');
        
        await channel.assertQueue(queue, {
            durable: false,
            arguments: {
                "x-dead-letter-exchange":"",
                "x-dead-letter-routing-key": DEAD_LETTERS_QUEUE_NAME
            }
        });
        // OPTIONAL :: Will also be done by the worker
        
        const corr = uuid();
        
        channel.sendToQueue(queue, new Buffer(JSON.stringify(payload)), {
            correlationId: corr
        });
        
        console.log(corr,'\nSent MSG on Queue:',queue,' Payload:',payload);
        
        resolve();

    });
}


module.exports.connectCommunicator = connect
module.exports.makeRPC = makeRPC
module.exports.fireTask = fireTask
module.exports.consumeQueue = consumeQueue
module.exports.callFunc = callFunc