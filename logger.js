const showLogs = !!process.env.COMM_LOGS

const log = (...args) => {
    if (showLogs) console.log(...args);
}

module.exports = {
    log
}