const Lambda = require('aws-sdk/clients/lambda');
const AWS = require('aws-sdk');
let lambda;
const logger = require('./logger')
const uuid = require('uuid/v4');

const getLambdaInstance = ({ region }) => {
    let _region = region || process.env.AWS_REGION || 'eu-west-1';
    // if lambda is not created or lambda is created and region is different then
        // create new instance, otherwise the old one is fine
    if(!lambda || (lambda && lambda.config.region !== _region)){
        lambda = new Lambda({ region: _region });
    }

    return lambda;
}

const callFunc = async (funcName, payload, fireNForget = false) => {
    const startTime = new Date().getTime();
    const reqId = uuid();
    logger.log('\nCOMM', 'Calling', payload.region, funcName, reqId, '\n', JSON.stringify(payload));
    // const clientContext = {source: 'internal-comm'}

    const InvocationType = fireNForget ? 'Event' : 'RequestResponse';
    lambda = getLambdaInstance({ region: payload.region });
    const data = await lambda.invoke({
        FunctionName: funcName,
        InvocationType,
        Payload: JSON.stringify({
            from: "internal-comm",
            body: JSON.stringify(payload)
        })
    }).promise();

    if (InvocationType == 'Event') return;

    const endTime = new Date().getTime();
    const executionTime = endTime - startTime;

    const response = JSON.parse(data.Payload);
    if (response.statusCode == 200) {
        logger.log('\nCOMM', executionTime + ' ms', 'Result', funcName, reqId);
        return JSON.parse(response.body).data;
    } else {
        logger.log('\nCOMM', executionTime + ' ms', 'Error', funcName, reqId, '\n', JSON.stringify(response));
        if(response.body) {
            throw JSON.parse(response.body).error;
        }
        else {
            throw response.errorMessage || "Something went wrong"
        }
    }


}
module.exports.callFunc = callFunc
